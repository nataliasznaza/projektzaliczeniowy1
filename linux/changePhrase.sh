#!/bin/bash
IFS="
"
# zadaję zmienne
PHRASE1="$1"
PHRASE2="$2"

	# pętla if sprawdza czy pierwszy parametr to -h lub --help
	# jeśli tak to wyświetla pomoc
	if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        	echo "Witaj,"
        	echo "Skrypt służy do zmiany wybranej frazy w pliku na inną,"
	        echo "również wybraną w plikach znajdujących się w bieżącym katalogu"
        	echo "i jego podkatalogach."
	        echo "Aby wywołać skrypt należy umieścić go w katalogu,"
        	echo "w którym chcemy dokonać zmian w plikach, a następnie:"
	        echo "Wpisać w terminal:"
        	echo "./changePhrase.sh 'fraza, którą chcesz zmienić' 'fraza na którą chcesz zmienić'"
	        echo "lub sh changePhrase.sh 'fraza, którą chcesz zmienić' 'fraza na którą chcesz zmienić'"
        	echo "skrypt zamieni wybraną przez Ciebie frazę na inną,"
	        echo "również przez Ciebie wybraną w bieżącym katalogu i jego podkatalogach."
        	echo "----------------------"
	        echo "PRZYKŁAD:"
	        echo "./changePhrase.sh jutro dzisiaj"
	        echo "Wynikiem tego będzie zamiana w plikach słowa jutro na słowo dzisiaj."
        	echo "----------------------"
		echo "UWAGA!"
        	echo "Skrypt jest wrażliwy na wielkość liter."

	# jeżeli nie jest spełniony powyższy warunek sprawdza czy
	# wprowadzono jakiekolwiek parametry, czy ich wartość równa się zero
	elif [ $# -eq 0 ]; then
		#jeśli nie są podane parametry to wyświetla poniższy komunikat
		 echo "Nie podałeś parametrów, przejdź do opcji pomoc."
	# jeżeli powyższe warunki nie zostały spełnione to sprawdza kolejny
	# czy wartość parametrów nie jest większa niż 2 
	 elif [ $# -gt 2 ]; then
		# jeśli jest większa niż 2 to wyświetla komunikat
		echo "Podałeś złe parametry, przejdź do opcji pomoc."

	else
		# jeżeli żaden z powyższych warunków nie jest spełniony
		# to wykonuje dla wszystkich znalezionych plików
			for file in $(find . -type f); do
				# jeśli znajdują się w katalogu i podkatalogach
				if [[ -f $file ]] && [[ -w $file ]]; then
					if [ $0 != "$file" ] && [ $0 != "./$file" ]; then
						# zmienia podany string z frazy pierwszej na drugą 
	                			sed -i -- 's/'$PHRASE1'/'$PHRASE2'/g' "$file"
   					fi
				fi
			done
	fi
