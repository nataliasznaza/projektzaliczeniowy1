#!/bin/bash
IFS="
"
# pętla if sprawdza czy pierwszy parametr to -h lub --help
# jeśli tak to wyświetla pomoc
if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
	 echo "Witaj,"
	 echo "Skrypt służy do zmiany wielkości liter nazw plików"
	 echo "w katalogu, w którym się znajduje."
	 echo "------------------------"
	 echo "Sposób wywołania:"
	 echo " ./changeFileCase.sh lower upper"
	 echo "jeśli skrypt ma dokonać zmiany małych liter na wielkie"
	 echo " ./changeFileCase.sh upper lower"
	 echo "jeśli skrypt ma dokonać zmiany wielkich liter na małe"
	 echo "Jeśli ma być dokonana zmiana nazwy tylko w konkretnym pliku"
	 echo "to należy dodać na końcu jeszcze nazwę tego pliku."
	 echo "------------------------"
	 echo "Przykład:"
	 echo " ./changeFileCase.sh lower upper tekstowy"
	 echo "Komenda ta zmieni litery małe na wielkie w pliku o nazwie: tekstowy"
	 echo "Jeżeli nazwa pliku zawiera spacje to należy wpisać ją w cudzysłowiu."

	 # jeżeli liczba wprowadzonych parametrów równa się zero
 	elif [ $# -eq 0 ]; then
		# to wyświetla poniższy komunikat
        	 echo "Nie podałeś parametrów, przejdź do opcji pomoc."
	# jeżeli liczba wprowadzonych parametrów równa się dwa to 
	elif [ $# -eq 2 ]; then 
		# jesli pierwszy parametr to lower a drugi upper to
		if [ "$1" = "lower" ] && [ "$2" = "upper" ]; then
			# wykonuje pętlę gdzie dla każdego pliku w katalogu zmienia nazwę 
			# z małych liter na wielkie (przenosi nazwę pliku do tego samego katalogu
			# przy jednoczesnej zmianie nazwy)
                	for file in * ; do
				# z wykluczeniem nazwy pliku 
				if [ $0 != "$file" ] && [ $0 != "./$file" ]; then
		 		mv "$file" "$(echo $file | tr [:$1:] [:$2:])";
    				fi
   			done
		fi
		# jesli pierwszy parametr to upper a drugi lower to
		if [ "$1" = "upper" ] && [ "$2" = "lower" ]; then
			# wykonuje pętlę gdzie dla każdego pliku w katalogu zmienia nazwę
                        # z wielkich liter na małe (przenosi nazwę pliku do tego samego katalogu
                        # przy jednoczesnej zmianie nazwy)
			for file in * ; do
				# z wykluczeniem nazwy pliku
                        	if [ $0 != "$file" ] && [ $0 != "./$file" ]; then
				mv "$file" "$(echo $file | tr [:$1:] [:$2:])";
                        	fi
                	done
		fi
	# jeżeli liczba wprowadzonych parametrów równa się trzy to
	 elif [ $# -eq 3 ]; then
		# jesli pierwszy parametr to lower a drugi upper to
		if [ "$1" = "lower" ] && [ "$2" = "upper" ]; then
			# wykonuje pętlę gdzie dla nazwy pliku w katalogu, podanej jako parametr trzeci
			for file in * ; do
				# z wykluczeniem nazwy skryptu
                                if [ $0 != "$file" ] && [ $0 != "./$file" ]; then
				# zmienia nazwę tego pliku z małych liter na wielkie (przenosi 
				# nazwę pliku do tego samego katalogu  przy jednoczesnej zmianie nazwy)
				mv "$3" "$(echo $3 | tr [:$1:] [:$2:])";
                                exit ;
				fi
                        done
		exit
		fi
	# jeśli pierwszy parametr to upper a drugi lower to
	 if [ "$1" = "upper" ] && [ "$2" = "lower" ]; then
		 	# wykonuje pętlę gdzie dla nazwy pliku w katalogu, podanej jako parametr trzeci
                        for file in * ; do
				# z wykluczeniem nazwy skryptu
                                if [ $0 != "$file" ] && [ $0 != "./$file" ]; then
				# zmienia nazwę tego pliku z wielkich liter na małe (przenosi
                                # nazwę pliku do tego samego katalogu  przy jednoczesnej zmianie nazwy)
                                mv "$3" "$(echo $3 | tr [:$1:] [:$2:])";
                                exit ;
				fi
                    	done
        	exit
		fi
	# jeżeli podano więcej niż trzy parametry to wyświetla komunikat
	elif [ $# -gt 3 ]; then
                echo "Podałeś złe parametry, przejdź do opcji pomoc."

fi
