#!/bin/bash

# zadaję zmienną NUMBER
NUMBER=1
	# pętla if sprawdza czy pierwszy parametr to -h lub --help
	# jeśli tak to wyświetla pomoc
	if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then

		echo "Witaj,"
		echo "Skrypt zmienia nazwy plików znajdujących się w bieżącym katalogu"
		echo "na nazwę zaczynającą się od kolejnego numeru + podkreślenie + nazwa pliku"
		echo "Aby wywołać skrypt należy wpisać:"
		echo "./setFileNumber.sh change"
		echo "lub"
		echo "sh setFileNumber.sh change"
		echo "Wynikiem działania jest zmiana wszystkich nazw plików" 
		echo "na zaczynające się od kolejnego numeru + podreślenie + nazwa pliku"
		echo "----------------"
		echo "PRZYKŁAD:"
		echo "Zakładając, że w katalogu znajdują się pliki o nazwach plik1 oraz test2"
		echo "Po wpisaniu w terminal:"
		echo "./setFileNumber.sh change"
		echo "Skrypt zmieni nazwy plików na: 1_plik1 oraz 2_test2"

	#jeżeli pierwszy parametr to słowo change wtedy
	elif  [ "$1" = "change" ]; then

		#pętla for zmienia dla każdego pliku w katalogu
		for file in * ; do
        		# z pominięciem nazwy skryptu
			if [ $0 != "$file" ] && [ $0 != "./$file" ]; then

        			# przenosi plik w to samo miejsce, ale ze zmienioną nazwą
        			# na numer + podkreślenie + nazwa pliku
				mv "${file}" "${NUMBER}_${file}"
	
				#następnie następuje zwiększenie liczby o +1
				#i wykonanie pętli ponownie
       				let NUMBER++
			fi
		done
	# jeśli nie został wykonany żaden z powyższych warunków to wyświetla komunikat
	else
        	echo "Podałeś błędy parametr, przejdź do opcji pomoc"
	fi
